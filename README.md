# README #

This repo contains a Unity project where I demonstrate all scripts and code snippets mentioned on my blog: https://grrava.blogspot.com

Instead of creating a different repo or single files for each post I'll gather all code in this project. Each topic gets a separate scene that demonstrates the code that is discussed in a post on the blog.

This project effectively deprecates the area triggers repository https://bitbucket.org/grrava/area-triggers, as development and changes on that code will be done in this repository.

### How do I get set up? ###

This project is a single Unity project. In the project there are folders per subject on the blog. At the time of writing that is:

* AreaTriggers - http://grrava.blogspot.be/2015/09/concave-triggers-in-unity-5.html
* ForwardTrigger - http://grrava.blogspot.be/2016/01/forwarding-trigger-events.html
* WorldBender - http://grrava.blogspot.be/2015/05/bending-world-with-unity.html
* Paint - http://grrava.blogspot.be/2016/04/drawing-game-for-mobile.html
* Stylized Fog - https://grrava.blogspot.com/2018/08/stylistic-fog-from-firewatch-with.html
* Gaussian blur is inside Stylized Fog - https://grrava.blogspot.com/2019/01/blur-with-unitys-post-fx-v2.html

Open the project with Unity and open any scene to view a demo. This project is compatible with Unity 2018.3.0f2