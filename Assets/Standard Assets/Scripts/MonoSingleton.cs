using UnityEngine;
public abstract class MonoSingleton<T> : MonoBehaviour where T : MonoSingleton<T>
{
	private static T m_Instance;
	private static bool hasBeenCreated;
	public static T Instance
	{
		get
		{
			// Instance requiered for the first time, we look for it
			if( m_Instance == null )
			{
				m_Instance = FindObjectOfType(typeof(T)) as T;
 
				// Object not found, we create one
				if (m_Instance == null && !hasBeenCreated)
				{
					m_Instance = new GameObject("_" + typeof(T), typeof(T)).GetComponent<T>();
				}
 
				// Problem during the creation, this should not happen
				if( m_Instance == null )
				{
#if !UNITY_EDITOR
					if(hasBeenCreated)
						Debug.LogError(string.Format("Singleton of type {0} has already been destroyed.", typeof(T).ToString()));
#endif
					if(!hasBeenCreated)
						Debug.LogError("Problem during the creation of " + typeof(T));
				}
				else
					DontDestroyOnLoad(m_Instance);
			}
			hasBeenCreated = true;
			return m_Instance;
		}
	}
	// If no other monobehaviour request the instance in an awake function
	// executing before this one, no need to search the object.
	private void Awake()
	{
		DontDestroyOnLoad (this);
		if( m_Instance == null )
		{
			m_Instance = this as T;
			hasBeenCreated = true;
		}
		m_Instance.Init();
	}
 
	// This function is called when the instance is used the first time
	// Put all the initializations you need here, as you would do in Awake
	public virtual void Init(){}

	// Use this method to ensure that the singleton is created.
	public void Touch(){}

	// Make sure the instance isn't referenced anymore when the user quit, just in case.
	private void OnApplicationQuit()
	{
		m_Instance = null;
	}
	
	protected void Kill()
	{
		m_Instance = null;
		Destroy(gameObject);
	}
}