using UnityEngine;

public static class Extensions 
{
	public static bool IsLowerCase(this string text)
	{
		if (string.IsNullOrEmpty(text)) { return true; }
		foreach (char c in text)
			if (char.IsLetter(c) && !char.IsLower(c))
				return false;

		return true;
	}

	public static Vector3 ToVector3(this string rString)
	{
		string[] temp = rString.Substring(1, rString.Length - 2).Split(',');
		float x = float.Parse(temp[0]);
		float y = float.Parse(temp[1]);
		float z = float.Parse(temp[2]);
		return new Vector3(x, y, z);
	}

	public static void ResetLocal(this Transform t)
	{
		t.localPosition = Vector3.zero;
		t.localRotation = Quaternion.identity;
		t.localScale = Vector3.one;
	}
}
