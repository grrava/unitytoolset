using System.Linq;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System.Collections.Generic;

[AddComponentMenu("Mesh/Combine Children")]
public class CombineChildren : MonoBehaviour {

#if UNITY_EDITOR
	private void Awake()
	{
		CheckIfStatic(gameObject);
		MeshRenderer[] meshRenderers = GetComponentsInChildren<MeshRenderer>();
		foreach (var meshRenderer in meshRenderers)
		{
			CheckIfStatic(meshRenderer.gameObject);
		}
	}

	private static void CheckIfStatic(GameObject go)
	{
		if (go.isStatic)
		{
			Debug.LogError(
				string.Format(
					"{0} is static! You can't have children of a parent with a CombineChildren component that are static",
					go.name), go);
			go.isStatic = false;
		}
	}
#endif

	private void Start()
	{
		Matrix4x4 myTransform = transform.worldToLocalMatrix;
		Dictionary<Material, List<CombineInstance>> combines = new Dictionary<Material, List<CombineInstance>>();
		MeshRenderer[] meshRenderers = GetComponentsInChildren<MeshRenderer>();
		foreach (var meshRenderer in meshRenderers)
		{
			foreach (var material in meshRenderer.sharedMaterials)
				if (material != null && !combines.ContainsKey(material))
					combines.Add(material, new List<CombineInstance>());
		}

		MeshFilter[] meshFilters = GetComponentsInChildren<MeshFilter>();
		foreach(var filter in meshFilters)
		{
			Renderer meshRenderer = filter.GetComponent<Renderer>();
			if (filter.sharedMesh == null)
				continue;
			if (meshRenderer.sharedMaterial == null)
			{
				Debug.LogError(string.Format("Found renderer without material: {0}", filter.gameObject.name), filter.gameObject);
				continue;
			}
			CombineInstance ci = new CombineInstance
			{
				mesh = filter.sharedMesh,
				transform = myTransform*filter.transform.localToWorldMatrix
			};
			combines[meshRenderer.sharedMaterial].Add(ci);
			Destroy(meshRenderer);
		}

		foreach(Material m in combines.Keys)
		{
			var go = new GameObject("Combined mesh");
			go.transform.parent = transform;
			go.transform.position = Vector3.zero;
			go.transform.rotation = Quaternion.identity;
			go.transform.localScale = Vector3.one;

			var filter = go.AddComponent<MeshFilter>();
			filter.mesh.CombineMeshes(combines[m].ToArray(), true, true);
	
			var arenderer = go.AddComponent<MeshRenderer>();
			arenderer.material = m;
		}
	}
}

#if UNITY_EDITOR
[InitializeOnLoad]
public class CombinedChildrenScenePreProcessor : UnityEditor.AssetModificationProcessor
{
	public static string[] OnWillSaveAssets(string[] paths)
	{
		foreach (string path in paths)
			if (path.Contains(".unity"))
			{
				var combineObjects = Object.FindObjectsOfType<CombineChildren>();
				foreach (var combineChild in combineObjects)
				{
					combineChild.gameObject.isStatic = false;
					foreach (Transform child in combineChild.transform.Cast<Transform>().Where(child => child.gameObject.isStatic))
						child.gameObject.isStatic = false;
				}
			}
		return paths;
	}
}
#endif