﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Unlit/PaintBrush"
{
	Properties
	{
		_Color		("Main Color", Color)	= (1, 1, 1, 1)
		_From		("From", Vector)		= (0, 0, 0, 0)
		_To			("To", Vector)			= (0, 0, 0, 0)
		_Rad		("Radius", Float)		= 1
		_Hardness	("Hardness", Float)		= 10
		_Size		("Size", Float)			= 1024
		_Outline	("Outline", 2D)			= "white" {}
	}

	SubShader
	{
		Tags { "RenderType"="Opaque" }
		Blend SrcAlpha OneMinusSrcAlpha, One One
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float4 color : COLOR;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				float2 pt : TEXCOORD0;
				float4 color : COLOR;
			};

			float4 _Color;
			float4 _From;
			float4 _To;
			float _Rad;
			float _Hardness;
			float _Size;
			sampler2D _Outline;

			float2 NearestPointStrict(float2 p1, float2 p2, float2 pt)
			{
				float dx = p2.x - p1.x;
				float dy = p2.y - p1.y;
				if ((abs(dx) < 0.0001) && (abs(dy) < 0.0001))
					return p1; // It's a point not a line segment.

				// Calculate the t that minimizes the distance.
				float t = ((pt.x - p1.x) * dx + (pt.y - p1.y) * dy) / (dx * dx + dy * dy);

				// See if this represents one of the segment's
				// end points or a point in the middle.
				if (t < 0)
					return p1;
				if (t > 1)
					return p2;
				return float2(p1.x + t * dx, p1.y + t * dy);
			}

			float GaussFalloff(float distance, float inRadius)
			{
				return clamp(pow(360, -pow(distance / inRadius, 2.5) - 0.001),0,1);
			}
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.pt = v.vertex.xy;
				o.color = v.color;
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 outline = tex2D(_Outline, i.pt/_Size);
				fixed4 col = i.color;
				float2 center = i.pt + float2(0.5, 0.5);
				float dist = length(center - NearestPointStrict(_From.xy, _To.xy, center));
				dist = GaussFalloff(dist, _Rad) * _Hardness;
				col.a = col.a * dist;
				col.rgb = col.rgb * outline.rgb;
				return col;
			}
			ENDCG
		}
	}
}
