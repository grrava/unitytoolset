﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Random = UnityEngine.Random;

public class Painter : MonoBehaviour, IEndDragHandler, IBeginDragHandler, IDragHandler, IPointerDownHandler
{
	private readonly Queue<FloodFillRange> ranges = new Queue<FloodFillRange>();
	private readonly List<FloodFillRange> rangePool = new List<FloodFillRange>();
	private const float tolerance = 0.05f;

	public RenderTexture drawing;
	public Texture2D original;
	public Color color = Color.gray;
	public float hardness = 3.0f;
	public float lineWidth = 20.0f;
	public Material bucketMaterial;
	public Material brushMaterial;
	public PaintTool tool;

	private Vector2 currentMousePosition;
	private Vector2 previousMousePosition;

	private RectTransform myRect;
	private Color[] oriPixels;
	private bool[] pixelsChecked;
	private PaintData data = new PaintData();
	private int size;

	private void Start()
	{
		size = original.width;
	    myRect = GetComponent<RectTransform>();
		Graphics.Blit(original, drawing);
		oriPixels = original.GetPixels();
		pixelsChecked = new bool[size * size];
	}

	private FloodFillRange GetFromPool(int startX, int endX, int y)
	{
		if(rangePool.Count > 0)
		{
			var result = rangePool[0];
			rangePool.RemoveAt(0);
			result.Init(startX, endX, y);
			return result;
		}
		return new FloodFillRange(startX, endX, y);
	}

	private void ReturnToPool(FloodFillRange range)
	{
		rangePool.Add(range);
	}

	/// <summary>
	/// Finds the furthermost left and right boundaries of the fill area
	/// on a given y coordinate, starting from a given x coordinate, filling as it goes.
	/// Adds the resulting horizontal range to the queue of floodfill ranges,
	/// to be processed in the main loop.
	/// </summary>
	/// <param name="x">The x coordinate to start from.</param>
	/// <param name="y">The y coordinate to check at.</param>
	private void LinearFill(int x, int y)
	{
		//***Find Left Edge of Color Area
		int lFillLoc = x; //the location to check/fill on the left
		int idx = (y * size) + x;
		while (true)
		{
			//**fill with the color
			if (idx < 0 || idx >= pixelsChecked.Length)
				break;
			if (!pixelsChecked[idx])
				pixelsChecked[idx] = true;
			//**de-increment
			--lFillLoc;
			--idx;
			//**exit loop if we're at edge of bitmap or color area
			if (lFillLoc <= data.stX || (pixelsChecked[idx]) || !CheckPixel(idx))
				break;
		}

		//***Find Right Edge of Color Area
		int rFillLoc = x; //the location to check/fill on the left
		idx = (y * size) + x;
		while (true)
		{
			if (idx < 0 || idx >= pixelsChecked.Length)
				break;
			if (!pixelsChecked[idx])
				pixelsChecked[idx] = true;
			//**increment
			++rFillLoc;     //increment counter
			++idx;
			//**exit loop if we're at edge of bitmap or color area
			if (rFillLoc >= data.endX || pixelsChecked[idx] || !CheckPixel(idx))
				break;
		}

		DrawLine(rFillLoc,lFillLoc,y,y);
		++lFillLoc;
		--rFillLoc;
		//add range to queue
		ranges.Enqueue(GetFromPool(lFillLoc, rFillLoc, y));
	}

	private void DrawLine(float x, float x2, float y, float y2)
	{
		GL.Begin(GL.LINES);
		GL.Color(color);
		GL.Vertex3(x, y+1, 0);
		GL.Vertex3(x2, y2+1, 0);
		GL.End();

        // we offset y with +1 because of a bug in rendertexture blitting:
        // https://issuetracker.unity3d.com/issues/game-view-is-offset-when-blitting-from-a-rendertexture
	}

	public void PaintLine(Vector2 from, Vector2 to, float rad)
	{
		data.SetData(from, to, rad, size);

		ranges.Clear();

		//***Get starting color.
		var x = (int)from.x; var y = (int)from.y;

		for (int i = (int)data.stX; i < data.endX; ++i)
			for (int j = (int)data.stY; j < data.endY; ++j)
				pixelsChecked[(j * size) + i] = false;

        RenderTexture.active = drawing;
		GL.PushMatrix();
		GL.LoadPixelMatrix(0, size, 0, size);
		if (tool == PaintTool.Bucket)
		{
			bucketMaterial.SetFloat("_Size", size);
			bucketMaterial.SetTexture("_Outline", original);
			bucketMaterial.SetPass(0);
		}
		else
		{
			brushMaterial.SetVector("_From", from);
			brushMaterial.SetVector("_To", to);
			brushMaterial.SetFloat("_Rad", rad);
			brushMaterial.SetFloat("_Hardness", hardness);
			brushMaterial.SetFloat("_Size", size);
			brushMaterial.SetTexture("_Outline", original);
			brushMaterial.SetPass(0);
		}

		//***Do first call to floodfill.
		LinearFill(x, y);

		//***Call floodfill routine while floodfill ranges still exist on the queue
		while (ranges.Count > 0)
		{
			//Get Next Range Off the Queue
			var range = ranges.Dequeue();

			//**Check Above and Below Each Pixel in the Floodfill Range
			int upPxIdx = (size * (range.Y + 1)) + range.StartX;
			int downPxIdx = upPxIdx - (2 * size);
			int downY = range.Y - 1;//so we can pass the y coord by ref
			int upY = downY + 2;
			for (int i = range.StartX; i <= range.EndX; ++i, upPxIdx++, downPxIdx++)
			{
				//Start Fill Downwards
				//if we're not below the bottom of the bitmap and the pixel below this one is within the color tolerance
				int tempIdx;
				if (range.Y > data.stY && (!pixelsChecked[downPxIdx]))
				{
					tempIdx = (downY * size) + i;
					if (CheckPixel(tempIdx))
						LinearFill(i, downY);
				}

				//Start Fill Upwards
				//if we're not above the top of the bitmap and the pixel above this one is within the color tolerance
				if (range.Y < (data.endY - 1) && (!pixelsChecked[upPxIdx]))
				{
					tempIdx = (upY * size) + i;
					if (CheckPixel(tempIdx))
						LinearFill(i, upY);
				}
			}
			ReturnToPool(range);

		}

		GL.PopMatrix();
		RenderTexture.active = null;
	}

	protected bool CheckPixel(int i)
	{
		return !(oriPixels[i].r < tolerance &&
				 oriPixels[i].g < tolerance &&
				 oriPixels[i].b < tolerance);
	}

	public void OnDrag(PointerEventData eventData)
	{
		SetCurrentMousePosition(eventData.position, eventData.enterEventCamera);
		Draw();
		eventData.Use();
	}

	public void OnBeginDrag(PointerEventData eventData)
	{
		SetCurrentMousePosition(eventData.position, eventData.enterEventCamera);
		previousMousePosition = currentMousePosition;
		eventData.Use();
	}

	public void OnEndDrag(PointerEventData eventData)
	{
		SetCurrentMousePosition(eventData.position, eventData.enterEventCamera);
		eventData.Use();
	}

	private void Draw()
	{
		if (currentMousePosition.x < 0 || currentMousePosition.x > size)
			return;
		if (currentMousePosition.y < 0 || currentMousePosition.y > size)
			return;

		if (tool == PaintTool.PaintBrush)
			PaintLine(previousMousePosition, currentMousePosition, lineWidth);
		else
			PaintLine(currentMousePosition, currentMousePosition, size*2);

		previousMousePosition = currentMousePosition;
	}

	public void SetCurrentMousePosition(Vector3 pos, Camera cam)
	{
		RectTransformUtility.ScreenPointToLocalPointInRectangle(myRect, pos, cam, out currentMousePosition);
		currentMousePosition *= size/myRect.rect.width;
	}

	public void OnPointerDown(PointerEventData eventData)
	{
		if (tool == PaintTool.Bucket)
		{
			SetCurrentMousePosition(eventData.position, eventData.enterEventCamera);
			Draw();
		}
		eventData.Use();
	}

	public void Bigger()
	{
		lineWidth = Math.Min(lineWidth + 5, 200);
	}

	public void Smaller()
	{
		lineWidth = Math.Max(lineWidth - 5, 5);
	}

	public void ChangeColor()
	{
		color = new Color(Random.value, Random.value, Random.value);
	}

	public void Bucket()
	{
		tool = PaintTool.Bucket;
	}

	public void Brush()
	{
		tool = PaintTool.PaintBrush;
	}

	public void Clear()
	{
		Graphics.Blit(original, drawing);
	}
}

public struct PaintData
{
	public Vector2 from;
	public Vector2 to;

	public float stY;
	public float stX;
	public float endY;
	public float endX;

	public float rad;

	public void SetData(Vector2 vFrom, Vector2 vTo, float vRad, float size)
	{
		from = vFrom;
		to = vTo;
		rad = vRad;
		stY = Mathf.Clamp(Mathf.Min(from.y, to.y) - rad, 0, size);
		stX = Mathf.Clamp(Mathf.Min(from.x, to.x) - rad, 0, size);
		endY = Mathf.Clamp(Mathf.Max(from.y, to.y) + rad, 0, size);
		endX = Mathf.Clamp(Mathf.Max(from.x, to.x) + rad, 0, size);
	}
}

public class FloodFillRange
{
	public int StartX;
	public int EndX;
	public int Y;

	public FloodFillRange(int startX, int endX, int y)
	{
		Init(startX, endX, y);
	}

	public void Init(int startX, int endX, int y)
	{
		StartX = startX;
		EndX = endX;
		Y = y;
	}
}

public enum PaintTool
{
	PaintBrush,
	Bucket
}
