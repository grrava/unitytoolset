﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Unlit/PaintBucket"
{
	Properties
	{
		_Color("Main Color", Color) = (1, 1, 1, 1)
		_Size("Size", Float) = 1024
		_Outline("Outline", 2D) = "white" {}

	}

	SubShader
	{
		Tags { "RenderType"="Opaque" }
		Blend SrcAlpha OneMinusSrcAlpha, One One
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float4 color : COLOR;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				float2 pt : TEXCOORD0;
				float4 color : COLOR;
			};

			float4 _Color;
			float _Size;
			sampler2D _Outline;

			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.pt = v.vertex.xy;
				o.color = v.color;
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 outline = tex2D(_Outline, i.pt / _Size);
				fixed4 col = i.color * outline;
				return col;
			}
			ENDCG
		}
	}
}
