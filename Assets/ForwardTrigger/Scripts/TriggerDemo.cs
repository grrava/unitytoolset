﻿using UnityEngine;

public class TriggerDemo : MonoBehaviour
{
    public GameObject[] cubes;

    public void OnCubeEnter(PhysicsTriggerEventData data)
    {
        var material = data.Receiver.GetComponent<Renderer>().material;
        foreach (var cube in cubes)
        {
            cube.GetComponent<Renderer>().material = material;
        }
    }
}
