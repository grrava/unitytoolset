﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[AddComponentMenu("Physics/Trigger Forwarder")]
public class ForwardTrigger : MonoBehaviour {

    [Serializable]
    public class TriggerEvent : UnityEvent<PhysicsTriggerEventData>
    {}

    [Serializable]
    public class Entry
    {
        public PhysicsTriggerType eventID = PhysicsTriggerType.TriggerEnter;
        public TriggerEvent callback = new TriggerEvent();
    }

    public List<Entry> delegates;

    private void Execute(PhysicsTriggerType id, PhysicsTriggerEventData eventData)
    {
        if (delegates != null)
        {
            for (int i = 0, imax = delegates.Count; i < imax; ++i)
            {
                var ent = delegates[i];
                if (ent.eventID == id && ent.callback != null)
                    ent.callback.Invoke(eventData);
            }
        }
    }

    private PhysicsTriggerEventData physicsTriggerEventData;
    private PhysicsTriggerEventData GetPhysicsTriggerEventData()
    {
        if(physicsTriggerEventData == null)
            physicsTriggerEventData = new PhysicsTriggerEventData(gameObject);
        return physicsTriggerEventData;
    }

    private void OnTriggerEnter(Collider other)
    {
        var data = GetPhysicsTriggerEventData();
        data.Other = other;
        Execute(PhysicsTriggerType.TriggerEnter, data);
    }

    private void OnTriggerExit(Collider other)
    {
        var data = GetPhysicsTriggerEventData();
        data.Other = other;
        Execute(PhysicsTriggerType.TriggerExit, data);
    }

    private void OnTriggerStay(Collider other)
    {
        var data = GetPhysicsTriggerEventData();
        data.Other = other;
        Execute(PhysicsTriggerType.TriggerStay, data);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        var data = GetPhysicsTriggerEventData();
        data.Other2D = other;
        Execute(PhysicsTriggerType.TriggerEnter, data);
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        var data = GetPhysicsTriggerEventData();
        data.Other2D = other;
        Execute(PhysicsTriggerType.TriggerExit, data);
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        var data = GetPhysicsTriggerEventData();
        data.Other2D = other;
        Execute(PhysicsTriggerType.TriggerStay, data);
    }
}

public class PhysicsTriggerEventData
{
    public PhysicsTriggerEventData(GameObject receiver)
    {
        Receiver = receiver;
    }

    public GameObject Receiver { get; private set; }
    public Collider Other { get; set; }
    public Collider2D Other2D { get; set; }
}

public enum PhysicsTriggerType
{
    TriggerEnter = 0,
    TriggerExit = 1,
    TriggerStay = 2,
}
