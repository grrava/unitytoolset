using UnityEngine;

[RequireComponent(typeof(Camera))]
public class FrustrumViewer : MonoBehaviour
{
#if UNITY_EDITOR
	private readonly Vector3[] nearCorners = new Vector3[4]; //Approx'd nearplane corners
	private readonly Vector3[] farCorners = new Vector3[4]; //Approx'd farplane corners

	private Camera zeCamera;
	private WorldBender worldBender;

	public bool renderCullFrustum;
	private void Start()
	{
		zeCamera = GetComponent<Camera>();
		worldBender = GetComponent<WorldBender>();
	}


	private void Update()
	{
		DrawFrustum(zeCamera);
	}

	private void DrawFrustum(Camera cam)
	{
		if (renderCullFrustum && worldBender != null)
		{
			float ar = cam.aspect;
			float fov = cam.fieldOfView;
			float viewPortHeight = Mathf.Tan(Mathf.Deg2Rad * fov * 0.5f);
			float viewPortwidth = viewPortHeight * ar;

			float newfov = fov * (1 + worldBender.extraCullHeight);
			float newheight = Mathf.Tan(Mathf.Deg2Rad * newfov * 0.5f);
			float newar = viewPortwidth / (newheight);

			cam.projectionMatrix = Matrix4x4.Perspective(newfov, newar, cam.nearClipPlane, cam.farClipPlane);
		}

		Plane[] camPlanes = GeometryUtility.CalculateFrustumPlanes(cam); //get planes from matrix
		Plane temp = camPlanes[1]; camPlanes[1] = camPlanes[2]; camPlanes[2] = temp; //swap [1] and [2] so the order is better for the loop
		for (int i = 0; i < 4; i++)
		{
			nearCorners[i] = Plane3Intersect(camPlanes[4], camPlanes[i], camPlanes[(i + 1) % 4]); //near corners on the created projection matrix
			farCorners[i] = Plane3Intersect(camPlanes[5], camPlanes[i], camPlanes[(i + 1) % 4]); //far corners on the created projection matrix
		}

		for (int i = 0; i < 4; i++)
		{
			Debug.DrawLine(nearCorners[i], nearCorners[(i + 1) % 4], Color.red, Time.deltaTime, true); //near corners on the created projection matrix
			Debug.DrawLine(farCorners[i], farCorners[(i + 1) % 4], Color.blue, Time.deltaTime, true); //far corners on the created projection matrix
			Debug.DrawLine(nearCorners[i], farCorners[i], Color.green, Time.deltaTime, true); //sides of the created projection matrix
		}
		cam.ResetProjectionMatrix();
	}

	private static Vector3 Plane3Intersect(Plane p1, Plane p2, Plane p3)
	{ //get the intersection point of 3 planes
		return ((-p1.distance * Vector3.Cross(p2.normal, p3.normal)) +
				(-p2.distance * Vector3.Cross(p3.normal, p1.normal)) +
				(-p3.distance * Vector3.Cross(p1.normal, p2.normal))) /
			(Vector3.Dot(p1.normal, Vector3.Cross(p2.normal, p3.normal)));
	}
#endif
}

