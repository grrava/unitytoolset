﻿using UnityEngine;

public class PlayerController : MonoBehaviour 
{
	public Vector3 Feet { get { return transform.position; } }
	public Vector3 Center { get { return transform.position + centerOffset; } }
	public Vector3 Head { get { return transform.position + headOffset; } }

	public float speed = 2.0f;

	private Vector3 headOffset = Vector3.zero;
	private Vector3 centerOffset = Vector3.zero;
	private CharacterController characterController;
	private Transform cameraTransform;

	private void Awake()
	{
		GameController.Instance.Player = this;

		characterController = gameObject.GetComponent<CharacterController>();
		centerOffset = characterController.bounds.center - transform.position;
		headOffset = centerOffset;
		headOffset.y = characterController.bounds.max.y - transform.position.y;

	}
	
	private void Start () {
		var bender = FindObjectOfType<WorldBender>();
		if (bender != null)
			bender.hero = gameObject;
		cameraTransform = GameController.Instance.PlayerCamera.transform;
	}
	
	private void Update () {
		float v = Input.GetAxisRaw("Vertical");
		float h = Input.GetAxisRaw("Horizontal");

		Vector3 targetDirection = Vector3.zero;

		// Forward vector relative to the camera along the x-z plane	
		Vector3 forward = cameraTransform.TransformDirection(Vector3.forward);
		forward.y = 0;
		forward = forward.normalized;
		if (forward.sqrMagnitude == 0) // the magnitude can become 0 when the camera is right above the player.
			forward.z = 1;

		// Right vector relative to the camera
		// Always orthogonal to the forward vector
		Vector3 right = new Vector3(forward.z, 0, -forward.x);

		targetDirection.x = (h * right.x + v * forward.x);
		targetDirection.y = -9.81f;
		targetDirection.z = (h * right.z + v * forward.z);
		bool isMoving = targetDirection.sqrMagnitude > 0.1f;


		if (isMoving)
		{
			characterController.Move(targetDirection*Time.deltaTime * speed);
		}
	}
}
