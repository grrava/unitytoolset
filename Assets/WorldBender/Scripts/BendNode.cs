﻿using System.Reflection;
using UnityEditor.ShaderGraph;
using UnityEngine;

[Title("Custom", "World Bend")]
public class BendNode : CodeFunctionNode
{
	public BendNode()
	{
		name = "World Bend";
	}

	protected override MethodInfo GetFunctionToConvert()
	{
		return GetType().GetMethod("Bend",
			BindingFlags.Static | BindingFlags.NonPublic);
	}

	static string Bend(
		[Slot(0, Binding.ObjectSpacePosition)] Vector3 v,
		[Slot(1, Binding.None)] Vector1 Horizon,
		[Slot(2, Binding.None)] Vector1 Spread,
		[Slot(3, Binding.None)] Vector1 Attenuate,
		[Slot(4, Binding.None)] out Vector3 Out)
	{
		Out = Vector3.zero;
		return @"
{
	Out = mul (unity_ObjectToWorld, v);
	float dist = max(0.0, abs(Horizon.x - Out.z) - Spread);
	Out.y -= dist * dist * Attenuate;
	Out = mul(unity_WorldToObject, Out);
}";
	}
}
