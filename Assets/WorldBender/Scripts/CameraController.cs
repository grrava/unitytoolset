﻿using UnityEngine;

public enum CameraFocus
{
	Feet,
	Center,
	Head
};

[RequireComponent(typeof(Camera))]
public class CameraController : MonoBehaviour
{
	public bool isXAxisFixed = false;
	public bool isYAxisFixed = false;
	public bool isZAxisFixed = false;

	public float distance = 3.0f;
	public float height = 1.5f;
	public float movementLag = 1.0f;
	public CameraFocus cameraFocus = CameraFocus.Center;

	private PlayerController player;

	private float targetHeight = float.MaxValue;


	private void Awake()
	{
		GameController.Instance.PlayerCamera = this;
	}

	private void Start ()
	{
		player = GameController.Instance.Player;
	}
	
	private void Apply(float lag)
	{
		if (player == null) return;

		Vector3 targetCenter = GetLookTarget();
		targetHeight = targetCenter.y + height;

		Vector3 desiredPosition = GetDesiredPosition(targetCenter);

		iTween.MoveUpdate(gameObject, desiredPosition, lag);
		iTween.LookUpdate(gameObject, targetCenter, lag);
	}

	private Vector3 GetDesiredPosition(Vector3 targetCenter)
	{
		Vector3 desiredDirection = transform.forward * distance;
		Vector3 desiredPosition = new Vector3(
			isXAxisFixed ? transform.position.x : targetCenter.x - desiredDirection.x,
			isYAxisFixed ? transform.position.y : targetHeight,
			isZAxisFixed ? transform.position.z : targetCenter.z - desiredDirection.z
			);
		desiredPosition.x = isXAxisFixed ? transform.position.x : desiredPosition.x;
		desiredPosition.y = isYAxisFixed ? transform.position.y : desiredPosition.y;
		desiredPosition.z = isZAxisFixed ? transform.position.z : desiredPosition.z;
		return desiredPosition;
	}

	void LateUpdate()
	{
		Apply(movementLag);
	}

	private Vector3 GetLookTarget()
	{
		Vector3 targetCenter;
		switch (cameraFocus)
		{
			case CameraFocus.Center:
				targetCenter = player.Center;
				break;
			case CameraFocus.Head:
				targetCenter = player.Head;
				break;
			default:
				targetCenter = player.Feet;
				break;
		}
		if (isXAxisFixed) targetCenter.x = 0.0f;
		if (isYAxisFixed) targetCenter.y = 0.0f;
		if (isZAxisFixed) targetCenter.z = -1.0f;
		return targetCenter;
	}

}
