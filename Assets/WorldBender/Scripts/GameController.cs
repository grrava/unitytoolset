﻿public class GameController : MonoSingleton<GameController>
{
	public CameraController PlayerCamera { get; set; }
	public PlayerController Player { get; set; }
}
