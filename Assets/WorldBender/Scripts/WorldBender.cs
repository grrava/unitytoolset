﻿using UnityEngine;

public class WorldBender : MonoBehaviour
{
	[Range(0, 0.5f)]
	public float extraCullHeight;

	public GameObject hero;
	public Camera _camera;

	[Range(0.0f, 1.0f)]
	public float attenuation = 0.0f;
	public float horizonOffset = 0.0f;
	public float spread = 0.0f;
	public float Horizon { get { return hero != null ? hero.transform.position.z + horizonOffset : 0; } }

	private void Update()
	{
		if (hero != null)
		{
			Shader.SetGlobalFloat("_HORIZON", hero.transform.position.z + horizonOffset);
			Shader.SetGlobalFloat("_SPREAD", spread);
			Shader.SetGlobalFloat("_ATTENUATE", attenuation);
		}
	}

	private void OnDestroy()
	{
		Shader.SetGlobalFloat("_ATTENUATE", 0);
		Shader.SetGlobalFloat("_SPREAD", 0);
		Shader.SetGlobalFloat("_HORIZON", 0);
	}

	private void OnApplicationQuit()
	{
		Shader.SetGlobalFloat("_ATTENUATE", 0);
		Shader.SetGlobalFloat("_SPREAD", 0);
		Shader.SetGlobalFloat("_HORIZON", 0);
	}

	private void Start()
	{
		if(_camera == null)
			_camera = GetComponent<Camera>();
	}

	private void OnPreCull()
	{
		Shader.SetGlobalMatrix("_Camera2World", _camera.cameraToWorldMatrix);
		Shader.SetGlobalMatrix("_World2Camera", _camera.worldToCameraMatrix);

		float ar = _camera.aspect;
		float fov = _camera.fieldOfView;
		float viewPortHeight = Mathf.Tan(Mathf.Deg2Rad * fov * 0.5f);
		float viewPortwidth = viewPortHeight * ar;

		float newfov = fov * (1 + extraCullHeight);
		float newheight = Mathf.Tan(Mathf.Deg2Rad * newfov * 0.5f);
		float newar = viewPortwidth / (newheight);

		_camera.projectionMatrix = Matrix4x4.Perspective(newfov, newar, _camera.nearClipPlane, _camera.farClipPlane);
	}

	private void OnPreRender()
	{
		_camera.ResetProjectionMatrix();
	}
}
