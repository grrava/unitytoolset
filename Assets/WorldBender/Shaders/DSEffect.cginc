#ifndef DSEFFECT_INCLUDED
#define DSEFFECT_INCLUDED

uniform float _HORIZON = 0.0f;
uniform float _ATTENUATE = 0.0f;
uniform float _SPREAD = 0.0f;

float4x4 _Camera2World;
float4x4 _World2Camera;

float4 DSEffect (float4 v)
{
	float4 t = mul (unity_ObjectToWorld, v);
	float dist = max(0, abs(_HORIZON - t.z) - _SPREAD);
	t.y -= dist * dist * _ATTENUATE;
	t.xyz = mul(unity_WorldToObject, t).xyz;
	return t;
}

float4 ParticleDSEffect (float4 v)
{
	float4 t = mul (_Camera2World, v);
	float dist = max(0, abs(_HORIZON - t.z) - _SPREAD);
	t.y -= dist * dist * _ATTENUATE;
	t.xyz = mul(_World2Camera, t).xyz;
	return t;
}

#endif