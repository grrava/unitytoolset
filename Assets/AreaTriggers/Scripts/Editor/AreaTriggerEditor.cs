using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(AreaCollider), true)]
public class AreaTriggerEditor : Editor 
{
	private static readonly GUIContent 
		pointContent = GUIContent.none;

	private static readonly GUIContent 
		insertContent = new GUIContent("+", "duplicate this point");

	private static readonly GUIContent 
		deleteContent = new GUIContent("-", "delete this point");

	private static readonly GUILayoutOption
		buttonWidth = GUILayout.MaxWidth(20f);

	private AreaCollider at;
	
	public void OnEnable()
	{
		at = target as AreaCollider;
	}
	
	public override void OnInspectorGUI()
	{
		Event e = Event.current;
		SerializedObject areaTrigger = new SerializedObject(target);
		SerializedProperty points = areaTrigger.FindProperty("coords");
		
		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.PrefixLabel("Display Handles");
		at.showHandles = EditorGUILayout.Toggle(at.showHandles);
		EditorGUILayout.EndHorizontal();

		GUILayout.Label("Points");
		for(int i = 0; i < points.arraySize; i++){
			EditorGUILayout.BeginHorizontal();
			SerializedProperty point = points.GetArrayElementAtIndex(i);
			EditorGUILayout.PropertyField(point, pointContent);
			
			if(GUILayout.Button(insertContent, EditorStyles.miniButtonLeft, buttonWidth)){
				points.InsertArrayElementAtIndex(i);
			}
			if(GUILayout.Button(deleteContent, EditorStyles.miniButtonRight, buttonWidth)){
				points.DeleteArrayElementAtIndex(i);
			}
			
			EditorGUILayout.EndHorizontal();
		}

		if(areaTrigger.ApplyModifiedProperties() ||
			(e.type == EventType.ValidateCommand &&
			e.commandName == "UndoRedoPerformed"))
		{
			at.UpdateArea();
		}
		
		if(GUI.changed)
			EditorUtility.SetDirty(target);

		bool isTrigger = at is AreaTrigger;
		if (GUILayout.Button("Convert to " + (isTrigger ? "Collider" : "Trigger")))
		{
			AreaCollider area =
				at.gameObject.AddComponent(isTrigger ? typeof(AreaCollider) : typeof(AreaTrigger))
				as AreaCollider;

			area.CopyValuesFrom(at);
			DestroyImmediate(at);
		}
	}

	public void OnSceneGUI()
	{
		Event e = Event.current;
	
		if(at.showHandles)
			at.UpdateHandles();

		HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));
		
		switch(e.type)
		{	
		case EventType.MouseDown:
			{
			
				if(e.button == 0 && e.control)
				{
					RaycastHit hit;
					Ray ray = HandleUtility.GUIPointToWorldRay(e.mousePosition);
					if(Physics.Raycast(ray, out hit))
					{
						at.AddCoord(hit.point);
					}
					e.Use();
				}
			}
			break;
		}
		if (GUI.changed)
			EditorUtility.SetDirty(target);
	}
}
