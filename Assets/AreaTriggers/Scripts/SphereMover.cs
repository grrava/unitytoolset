using UnityEngine;

public class SphereMover : MonoBehaviour 
{
	public float radius = 10.0f;
	
	private float inc;

	void Update () 
	{
		inc += 0.025f;
		if(inc > Mathf.PI * 2)
			inc -= Mathf.PI * 2;
		transform.position = new Vector3(Mathf.Sin(inc) * radius,5.0f,Mathf.Cos(inc) * radius);	
	}
}
