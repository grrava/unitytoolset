using UnityEngine;

public class DemoTrigger : MonoBehaviour {
	
	private int counter;
	
	public void OnAreaTriggerEnter (Collider other) {
		Debug.Log (string.Format ("{0} Area Enter: {1} {2}", counter++, other.name, other.transform.position));
	}
	
	public void OnAreaTriggerExit (Collider other) {
		Debug.Log (string.Format ("{0} Area Exit: {1} {2}", counter++, other.name, other.transform.position));
	}

	public void OnAreaTriggerStay (Collider other) {
		Debug.Log ("Area Stay: " + other.name);
	}

}
